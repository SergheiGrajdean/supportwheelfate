﻿using System;
using System.Collections.Generic;

namespace SupportWheelFate.Domain.Models
{
    public class Day
    {
        public Day()
        {
            Shifts = new List<EngineerDayShift>();
        }

        public int Id { get; set; }

        public int SprintId { get; set; }

        public DateTimeOffset Date { get; set; }

        public List<EngineerDayShift> Shifts { get; set; }
    }
}
