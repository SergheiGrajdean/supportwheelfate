﻿namespace SupportWheelFate.Domain.Models
{
    public class EngineerDayShift
    {
        public int Id { get; set; }

        public int ShiftId { get; set; }

        public Shift Shift { get; set; }

        public int EngineerId { get; set; }

        public Engineer Engineer { get; set; }

        public int DayId { get; set; }
    }
}
