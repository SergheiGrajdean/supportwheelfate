﻿using System;
using System.Collections.Generic;

namespace SupportWheelFate.Domain.Models
{
    public class Sprint
    {
        public Sprint()
        {
            Days = new List<Day>();
        }

        public int Id { get; set; }

        public DateTimeOffset StartDate { get; set; }

        public DateTimeOffset EndDate { get; set; }

        public List<Day> Days { get; set; }
    }
}
