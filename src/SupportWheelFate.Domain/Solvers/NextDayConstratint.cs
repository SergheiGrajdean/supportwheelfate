﻿using SupportWheelFate.Domain.Abstract.Solvers;

namespace SupportWheelFate.Domain.Solvers
{
    public class NextDayConstratint : IConstraint
    {
        public int[] Filter(int[][] solution, int x, int y)
        {
            return x == solution.Length - 1 ? new int[0] : solution[x + 1];
        }
    }
}
