﻿using SupportWheelFate.Domain.Abstract.Solvers;

namespace SupportWheelFate.Domain.Solvers
{
    public class PreviousDayConstratint : IConstraint
    {
        public int[] Filter(int[][] solution, int x, int y)
        {
            return x == 0 ? new int[0] : solution[x - 1];
        }
    }
}
