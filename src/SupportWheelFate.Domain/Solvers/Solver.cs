﻿using System;
using System.Collections.Generic;
using System.Linq;
using SupportWheelFate.Domain.Abstract.Solvers;

namespace SupportWheelFate.Domain.Solvers
{
    public class Solver : ISolver
    {
        private readonly Random random = new Random();
        private readonly IList<IConstraint> constraints;

        public Solver(IList<IConstraint> constraints)
        {
            this.constraints = constraints;
        }

        public int[][] Solve(int shiftsPerDay, int numberOfDays, int numberOfEmployees)
        {
            int[][] solution = new int[numberOfDays][];
            var engineers = Enumerable.Range(0, numberOfEmployees).ToArray();

            Dictionary<(int, int), List<int>> tabuList = new Dictionary<(int, int), List<int>>();

            for (int i = 0; i < numberOfDays; i++)
            {
                solution[i] = new int[shiftsPerDay];
                for (int j = 0; j < shiftsPerDay; j++)
                {
                    solution[i][j] = -1;
                    tabuList[(i, j)] = new List<int>();
                }
            }

            for (int i = 0; i < shiftsPerDay; i++)
            {
                var availableEngineers = new List<int>(engineers);
                for (int j = 0; j < numberOfDays; j++)
                {
                    var currentDayEngineers = solution[j];
                    var previousDayEngineers = j == 0 ? new int[0] : solution[j - 1];
                    var nextDayEngineers = j == numberOfDays - 1 ? new int[0] : solution[j + 1];
                    
                    var filterQuery = availableEngineers.Except(tabuList[(j, i)]);
                    foreach (var constraint in this.constraints)
                    {
                        var filterList = constraint.Filter(solution, j, i);
                        filterQuery = filterQuery.Except(filterList);
                    }

                    var dayAvailableEngineers = filterQuery.ToList();

                    if (dayAvailableEngineers.Count == 0)
                    {
                        if (j == 0)
                        {
                            throw new NoFeasibleSolutionException("Unable to find solution for given arguments");
                        }

                        tabuList[(j - 1, i)].Add(solution[j - 1][i]);
                        tabuList[(j, i)].Clear();
                        availableEngineers.Add(solution[j - 1][i]);
                        solution[j - 1][i] = -1;
                        j -= 2;
                        continue;
                    }

                    var index = this.random.Next(dayAvailableEngineers.Count - 1);

                    var engineer = dayAvailableEngineers[index];
                    availableEngineers.Remove(engineer);
                    solution[j][i] = engineer;
                }
            }

            return solution;
        }
    }
}
