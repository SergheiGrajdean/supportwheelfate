﻿using System;
using System.Runtime.Serialization;

namespace SupportWheelFate.Domain.Solvers
{
    public class NoFeasibleSolutionException : Exception
    {
        public NoFeasibleSolutionException()
        {
        }

        public NoFeasibleSolutionException(string message): base(message)
        {

        }

        public NoFeasibleSolutionException(string message, Exception innerException)
            : base(message, innerException)
        {

        }

        public NoFeasibleSolutionException(SerializationInfo info, StreamingContext context)
            :base(info, context)
        {

        }

    }
}
