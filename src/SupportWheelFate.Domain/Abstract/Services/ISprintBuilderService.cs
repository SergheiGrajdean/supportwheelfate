﻿using System;
using SupportWheelFate.Domain.Models;

namespace SupportWheelFate.Domain.Abstract.Services
{
    public interface ISprintBuilderService
    {
        Sprint CreateSprint(DateTimeOffset startDate, DateTimeOffset endDate);
    }
}
