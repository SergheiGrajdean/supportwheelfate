﻿using System;
using System.Collections.Generic;
using SupportWheelFate.Domain.Models;

namespace SupportWheelFate.Domain.Abstract.Services
{
    public interface IWheelService
    {
        Sprint BuildSprint(IList<DateTimeOffset> dates, IList<Shift> shifts, IList<Engineer> engineers);
    }
}
