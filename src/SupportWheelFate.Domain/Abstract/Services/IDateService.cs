﻿using System;
using System.Collections.Generic;

namespace SupportWheelFate.Domain.Abstract.Services
{
    public interface IDateService
    {
        List<DateTimeOffset> GetDatesBetween(DateTimeOffset startDate, DateTimeOffset endDate, bool includeWeekends = false);
    }
}
