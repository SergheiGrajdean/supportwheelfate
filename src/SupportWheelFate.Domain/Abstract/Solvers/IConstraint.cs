﻿namespace SupportWheelFate.Domain.Abstract.Solvers
{
    public interface IConstraint
    {
        int[] Filter(int[][] solution, int x, int y);
    }
}
