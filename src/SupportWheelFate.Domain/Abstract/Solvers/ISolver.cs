﻿namespace SupportWheelFate.Domain.Abstract.Solvers
{
    public interface ISolver
    {
        int[][] Solve(int shiftsPerDay, int numberOfDays, int numberOfEmployees);
    }
}
