﻿using System;
using System.Linq;
using SupportWheelFate.Domain.Abstract.Services;
using SupportWheelFate.Domain.Data;
using SupportWheelFate.Domain.Models;

namespace SupportWheelFate.Domain.Services
{
    public class SprintBuilderService : ISprintBuilderService
    {
        private readonly IWheelService wheelService;
        private readonly IDateService dateService;
        private readonly SupportWheelContext supportWheelContext;

        public SprintBuilderService(IWheelService wheelService, IDateService dateService, SupportWheelContext supportWheelDbContext)
        {
            this.wheelService = wheelService;
            this.dateService = dateService;
            this.supportWheelContext = supportWheelDbContext;
        }

        public Sprint CreateSprint(DateTimeOffset startDate, DateTimeOffset endDate)
        {
            var dates = this.dateService.GetDatesBetween(startDate, endDate);
            var engineers = this.supportWheelContext.Engineers.OrderBy(x => x.Id).Take(dates.Count).ToList();
            var shifts = this.supportWheelContext.Shifts.ToList();

            var sprint = this.wheelService.BuildSprint(dates, shifts, engineers);
            sprint.StartDate = startDate;
            sprint.EndDate = endDate;


            this.supportWheelContext.Sprints.Add(sprint);
            this.supportWheelContext.SaveChanges();

            return sprint;
        }
    }
}
