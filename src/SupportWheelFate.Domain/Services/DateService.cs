﻿using System;
using System.Collections.Generic;
using System.Linq;
using SupportWheelFate.Domain.Abstract.Services;

namespace SupportWheelFate.Domain.Services
{
    public class DateService : IDateService
    {
        public List<DateTimeOffset> GetDatesBetween(DateTimeOffset startDate, DateTimeOffset endDate, bool includeWeekends = false)
        {
            var queryGenerator = Enumerable.Range(0, 1 + endDate.Subtract(startDate).Days)
                                           .Select(offset => startDate.AddDays(offset));
            if (!includeWeekends)
            {
                queryGenerator = queryGenerator.Where(x => x.DayOfWeek != DayOfWeek.Saturday && x.DayOfWeek != DayOfWeek.Sunday);
            }

            return queryGenerator.ToList();
        }
    }
}
