﻿using System;
using System.Collections.Generic;
using System.Linq;
using SupportWheelFate.Domain.Abstract.Services;
using SupportWheelFate.Domain.Abstract.Solvers;
using SupportWheelFate.Domain.Models;

namespace SupportWheelFate.Domain.Services
{
    public class WheelService : IWheelService
    {
        private readonly ISolver solver;

        public WheelService(ISolver solver)
        {
            this.solver = solver;
        }

        public Sprint BuildSprint(IList<DateTimeOffset> dates, IList<Shift> shifts, IList<Engineer> engineers)
        {
            var numberOfDays = dates.Count;
            var numberOfEmployees = engineers.Count;
            var shiftsPerDay = shifts.Count;

            var solution = this.solver.Solve(shiftsPerDay, numberOfDays, numberOfEmployees);

            var days = solution.Zip(dates, (daySolution, date) =>
            {
                var day = new Day { Date = date };
                var selectedEmployes = daySolution.Select(x => engineers[x]).ToList();
                day.Shifts = selectedEmployes.Zip(shifts, (engineer, shift) => new EngineerDayShift { Engineer = engineer, Shift = shift }).ToList();
                return day;
            }).ToList();

            var sprint = new Sprint
            {
                Days = days
            };

            return sprint;
        }
    }
}
