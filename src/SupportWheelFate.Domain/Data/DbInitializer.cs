﻿using System;
using System.Collections.Generic;
using System.Linq;
using SupportWheelFate.Domain.Models;

namespace SupportWheelFate.Domain.Data
{
    public static class DbInitializer
    {
        public static void Initialize(SupportWheelContext context)
        {
            context.Database.EnsureCreated();

            if (context.Engineers.Any())
            {
                return;
            }

            var engineers = new List<Engineer>
            {
                new Engineer { Name = "Charles Eugene" },
                new Engineer { Name = "William Thompson" },
                new Engineer { Name = "Carl Oscar" },
                new Engineer { Name = "William Augustus" },
                new Engineer { Name = "Mary Agatha" },
                new Engineer { Name = "Ernest Charles" },
                new Engineer { Name = "Jacques Heath" },
                new Engineer { Name = "William Henry" },
                new Engineer { Name = "Gosta Leonard" },
                new Engineer { Name = "Marguerite Rut" },
            };

            var shifts = new List<Shift>
            {
                new Shift { Start = TimeSpan.FromHours(9), End = TimeSpan.FromHours(13) },
                new Shift { Start = TimeSpan.FromHours(13), End = TimeSpan.FromHours(17) },
            };

            context.Engineers.AddRange(engineers);
            context.Shifts.AddRange(shifts);

            context.SaveChanges();
        }
    }
}
