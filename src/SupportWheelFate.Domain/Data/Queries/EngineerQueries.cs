﻿using System.Collections.Generic;
using System.Linq;
using SupportWheelFate.Domain.Models;

namespace SupportWheelFate.Domain.Data.Queries
{
    public static class EngineerQueries
    {
        public static ICollection<Engineer> GetEngineersBySprint(this SupportWheelContext context, int sprintId)
        {
            var query = from engineer in context.Engineers
                        join engineerDayShift in context.EngineerDayShifts on engineer.Id equals engineerDayShift.EngineerId
                        join day in context.Days on engineerDayShift.DayId equals day.Id
                        where day.SprintId == sprintId
                        orderby engineer.Id
                        select engineer;

            return query.Distinct().ToList();
        }
    }
}
