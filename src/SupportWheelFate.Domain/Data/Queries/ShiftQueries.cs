﻿using System.Collections.Generic;
using System.Linq;
using SupportWheelFate.Domain.Models;

namespace SupportWheelFate.Domain.Data.Queries
{
    public static class ShiftQueries
    {
        public static ICollection<Shift> GetShiftsBySprint(this SupportWheelContext context, int sprintId)
        {
            var query = from shift in context.Shifts
                        join engineerDayShift in context.EngineerDayShifts on shift.Id equals engineerDayShift.EngineerId
                        join day in context.Days on engineerDayShift.DayId equals day.Id
                        where day.SprintId == sprintId
                        orderby shift.Id
                        select shift;

            return query.Distinct().ToList();
        }
    }
}
