﻿using Microsoft.EntityFrameworkCore;
using SupportWheelFate.Domain.Models;

namespace SupportWheelFate.Domain.Data
{
    public class SupportWheelContext : DbContext
    {
        public SupportWheelContext(DbContextOptions<SupportWheelContext> options) : base(options)
        {
        }

        public DbSet<Engineer> Engineers { get; set; }
        public DbSet<Shift> Shifts { get; set; }
        public DbSet<EngineerDayShift> EngineerDayShifts { get; set; }
        public DbSet<Day> Days { get; set; }
        public DbSet<Sprint> Sprints { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Shift>();
            modelBuilder.Entity<Engineer>();

            modelBuilder.Entity<EngineerDayShift>()
                .HasOne(x => x.Engineer)
                .WithMany()
                .HasForeignKey(x => x.EngineerId);

            modelBuilder.Entity<EngineerDayShift>()
                .HasOne(x => x.Shift)
                .WithMany()
                .HasForeignKey(x => x.ShiftId);

            modelBuilder.Entity<Day>()
                .HasMany(x => x.Shifts)
                .WithOne()
                .HasForeignKey(x=>x.DayId);

            modelBuilder.Entity<Sprint>()
                .HasMany(x => x.Days)
                .WithOne()
                .HasForeignKey(x=>x.SprintId);
        }
    }
}
