﻿using System;

namespace SupportWheelFate.Api.Models
{
    public class CreateSprintRequest
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
    }
}
