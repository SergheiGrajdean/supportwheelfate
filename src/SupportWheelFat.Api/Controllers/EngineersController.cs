﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using SupportWheelFate.Domain.Data;
using SupportWheelFate.Domain.Models;

namespace SupportWheelFate.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/engineers")]
    public class EngineersController : Controller
    {
        private readonly SupportWheelContext context;

        public EngineersController(SupportWheelContext context)
        {
            this.context = context;
        }

        [HttpGet]
        public IEnumerable<Engineer> GetEngineers()
        {
            return context.Engineers;
        }
    }
}