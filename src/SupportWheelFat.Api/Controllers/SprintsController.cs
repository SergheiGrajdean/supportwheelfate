﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SupportWheelFate.Api.Models;
using SupportWheelFate.Domain.Abstract.Services;
using SupportWheelFate.Domain.Data;
using SupportWheelFate.Domain.Data.Queries;
using SupportWheelFate.Domain.Models;
using SupportWheelFate.Domain.Solvers;

namespace SupportWheelFate.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/Sprints")]
    public class SprintsController : Controller
    {
        private readonly SupportWheelContext context;
        private readonly ISprintBuilderService sprintBuilderService;

        public SprintsController(SupportWheelContext context, ISprintBuilderService sprintBuilderService)
        {
            this.context = context;
            this.sprintBuilderService = sprintBuilderService;
        }

        // GET: api/Sprints
        [HttpGet]
        public IEnumerable<Sprint> GetSprints()
        {
            return context.Sprints;
        }

        // GET: api/Sprints/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSprint([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var sprint = await context.Sprints
                .Include(x=>x.Days)
                .ThenInclude(x=>x.Shifts)
                .SingleOrDefaultAsync(m => m.Id == id);

            if (sprint == null)
            {
                return NotFound();
            }

            return Ok(sprint);
        }

        // GET: api/Engineers
        [HttpGet("{sprintId}/Engineers")]
        public IActionResult GetEngineers(int sprintId)
        {
            if (!this.context.Sprints.Any(x => x.Id == sprintId))
            {
                return NotFound();
            }

            var engineers = this.context.GetEngineersBySprint(sprintId);

            return Ok(engineers);
        }

        // GET: api/Engineers
        [HttpGet("{sprintId}/Shifts")]
        public IActionResult GetShifts(int sprintId)
        {
            if (!this.context.Sprints.Any(x => x.Id == sprintId))
            {
                return NotFound();
            }

            var shifts = this.context.GetShiftsBySprint(sprintId);

            return Ok(shifts);
        }

        // POST: api/Sprints
        [HttpPost]
        public IActionResult PostSprint([FromBody] CreateSprintRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var sprint = this.sprintBuilderService.CreateSprint(request.StartDate, request.EndDate);
                return CreatedAtAction("GetSprint", new { id = sprint.Id }, sprint);
            }
            catch (NoFeasibleSolutionException)
            {
                ModelState.AddModelError("dateRange", "Uanble to create sprint for given period. Please try longer period.");
                return BadRequest(ModelState);
            }
        }
    }
}