﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SupportWheelFate.Api.Models;

namespace SupportWheelFate.Api.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        // GET api/values
        [HttpGet]
        public IActionResult Get()
        {
            var m = new CreateSprintRequest
            {
                StartDate = DateTime.Parse("12/18/2017", CultureInfo.InvariantCulture),
                EndDate = DateTime.Parse("12/30/2017", CultureInfo.InvariantCulture),
            };

            var r = new List<DateTimeOffset>
            {
                DateTimeOffset.Parse("12/18/2017", CultureInfo.InvariantCulture),
                DateTimeOffset.Parse("12/30/2017", CultureInfo.InvariantCulture),
            };

            return Ok(m);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
