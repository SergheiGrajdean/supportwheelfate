﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using SupportWheelFate.Domain.Data;
using SupportWheelFate.Domain.Models;

namespace SupportWheelFate.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/shifts")]
    public class ShiftsController : Controller
    {
        private readonly SupportWheelContext context;

        public ShiftsController(SupportWheelContext context)
        {
            this.context = context;
        }

        [HttpGet]
        public IEnumerable<Shift> GetShifts()
        {
            return context.Shifts;
        }
    }
}