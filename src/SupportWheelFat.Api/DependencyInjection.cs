﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using SupportWheelFate.Domain.Abstract.Services;
using SupportWheelFate.Domain.Abstract.Solvers;
using SupportWheelFate.Domain.Services;
using SupportWheelFate.Domain.Solvers;

namespace SupportWheelFate.Api
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddDomainServices(this IServiceCollection services)
        {
            services.AddTransient<ISprintBuilderService, SprintBuilderService>();
            services.AddTransient<IDateService, DateService>();
            services.AddTransient<IWheelService, WheelService>();
            services.AddTransient<ISolver, Solver>(CreateSolver);

            return services;
        }

        private static Solver CreateSolver(IServiceProvider provider)
        {
            var constraints = new List<IConstraint>
            {
                new PreviousDayConstratint(),
                new CurrenttDayConstratint(),
                new NextDayConstratint()
            };

            return new Solver(constraints);
        }
    }
}
