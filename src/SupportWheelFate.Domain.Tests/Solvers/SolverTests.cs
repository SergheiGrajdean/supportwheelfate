﻿using System.Collections.Generic;
using NUnit.Framework;
using SupportWheelFate.Domain.Abstract.Solvers;
using SupportWheelFate.Domain.Solvers;

namespace SupportWheelFate.Domain.Tests.Solvers
{
    [TestFixture]
    public class SolverTests
    {
        private Solver solver;

        [SetUp]
        public void SetUp()
        {
            var constraints = new List<IConstraint>
            {
                new CurrenttDayConstratint(),
                new NextDayConstratint(),
                new PreviousDayConstratint()
            };

            this.solver = new Solver(constraints);
        }

        [Test]
        public void Solve_ValidData_FindsSolution()
        {
            // Arrange
            var numberOfShifts = 2;
            var numberOfDays = 10;
            var numberOfEngineers = 10;
            // Act
            var solution = this.solver.Solve(numberOfShifts, numberOfDays, numberOfEngineers);

            // Assert
            Assert.IsNotNull(solution);
            Assert.AreEqual(numberOfDays, solution.Length);
            foreach (var shift in solution)
            {
                Assert.IsNotNull(shift);
                Assert.AreEqual(numberOfShifts, solution[0].Length);
            }

            for (int i = 0; i < numberOfDays; i++)
            {
                CollectionAssert.AllItemsAreUnique(solution[i], "Several items with same value in a column");
                for (int j = 0; j < numberOfShifts; j++)
                {
                    if (i != 0)
                    {
                        CollectionAssert.DoesNotContain(solution[i - 1], solution[i][j], "Same item found in previous column");
                    }

                    if (i < numberOfDays - 1)
                    {
                        CollectionAssert.DoesNotContain(solution[i + 1], solution[i][j], "Same item found in next column");
                    }
                }
            }
        }
    }
}
