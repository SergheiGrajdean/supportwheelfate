﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AutoFixture;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using SupportWheelFate.Domain.Abstract.Services;
using SupportWheelFate.Domain.Data;
using SupportWheelFate.Domain.Models;
using SupportWheelFate.Domain.Services;

namespace SupportWheelFate.Domain.Tests.Services
{
    [TestFixture]
    public class SprintBuilderServiceTests
    {
        private IFixture fixture;

        [SetUp]
        public void SetUp()
        {
            this.fixture = new Fixture();
            this.fixture.Behaviors
                .OfType<ThrowingRecursionBehavior>()
                .ToList()
                .ForEach(b => this.fixture.Behaviors.Remove(b));

            this.fixture.Behaviors.Add(new OmitOnRecursionBehavior());
        }

        [Test]
        public void CreateSprint_ValidData_SavesSprint()
        {
            // Arrange
            var start = DateTimeOffset.Parse("01/25/2017", CultureInfo.InvariantCulture);
            var end = DateTimeOffset.Parse("01/27/2017", CultureInfo.InvariantCulture);

            var options = new DbContextOptionsBuilder<SupportWheelContext>()
                .UseInMemoryDatabase(databaseName: "CreateSprint_ValidData_SavesSprint")
                .Options;

            var expectedSprint = this.fixture.Create<Sprint>();

            var wheelService = this.fixture.Freeze<Mock<IWheelService>>();
            var dateService = new DateService();

            wheelService.Setup(x => x.BuildSprint(It.IsAny<IList<DateTimeOffset>>(), It.IsAny<IList<Shift>>(), It.IsAny<IList<Engineer>>()))
                .Returns(expectedSprint);

            // Act
            Sprint actualSprint;
            using (var context = new SupportWheelContext(options))
            {
                var sprintService = new SprintBuilderService(wheelService.Object, dateService, context);
                actualSprint = sprintService.CreateSprint(start, end);
            }

            // Assert
            Assert.AreSame(expectedSprint, actualSprint);
            using (var context = new SupportWheelContext(options))
            {
                Assert.AreEqual(1, context.Sprints.Count());
                Assert.AreEqual(expectedSprint.Id, context.Sprints.Single().Id);
            }
        }
    }
}
