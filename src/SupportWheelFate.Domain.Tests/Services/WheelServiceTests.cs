﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AutoFixture;
using Moq;
using NUnit.Framework;
using SupportWheelFate.Domain.Abstract.Solvers;
using SupportWheelFate.Domain.Models;
using SupportWheelFate.Domain.Services;

namespace SupportWheelFate.Domain.Tests.Services
{
    [TestFixture]
    public class WheelServiceTests
    {
        private WheelService wheelService;
        private Mock<ISolver> solver;
        private Fixture fixture;

        [SetUp]
        public void SetUp()
        {
            this.fixture = new Fixture();
            this.solver = this.fixture.Freeze<Mock<ISolver>>();
            this.wheelService = new WheelService(this.solver.Object);
        }

        [Test]
        public void BuildSprint_ValidData_BuildsSprint()
        {
            // Arrange
            var firstDay = DateTimeOffset.Parse("01/25/2017", CultureInfo.InvariantCulture);
            var secondDay = DateTimeOffset.Parse("01/26/2017", CultureInfo.InvariantCulture);
            var thirdDay = DateTimeOffset.Parse("01/27/2017", CultureInfo.InvariantCulture);
            var fourthDay = DateTimeOffset.Parse("01/28/2017", CultureInfo.InvariantCulture);
            var dates = new List<DateTimeOffset> { firstDay, secondDay, thirdDay, fourthDay };

            var shiftsNumber = 2;

            var shifts = this.fixture.CreateMany<Shift>(shiftsNumber).ToList();

            var engineers = this.fixture.CreateMany<Engineer>(dates.Count).ToList();

            var solution = new int[][]
            {
                new int[] { 0, 2 },
                new int[] { 3, 1 },
                new int[] { 2, 0 },
                new int[] { 1, 3 },
            };

            this.solver.Setup(x => x.Solve(shiftsNumber, dates.Count, dates.Count)).Returns(solution);

            // Act
            var sprint = this.wheelService.BuildSprint(dates, shifts, engineers);

            // Assert
            Assert.IsNotNull(sprint);
            Assert.IsNotNull(sprint.Days);
            Assert.AreEqual(dates.Count, sprint.Days.Count);

            Assert.AreSame(sprint.Days[0].Shifts[0].Engineer, engineers[0]);
            Assert.AreSame(sprint.Days[0].Shifts[1].Engineer, engineers[2]);
            Assert.AreSame(sprint.Days[1].Shifts[0].Engineer, engineers[3]);
            Assert.AreSame(sprint.Days[1].Shifts[1].Engineer, engineers[1]);
            Assert.AreSame(sprint.Days[2].Shifts[0].Engineer, engineers[2]);
            Assert.AreSame(sprint.Days[2].Shifts[1].Engineer, engineers[0]);
            Assert.AreSame(sprint.Days[3].Shifts[0].Engineer, engineers[1]);
            Assert.AreSame(sprint.Days[3].Shifts[1].Engineer, engineers[3]);
        }
    }
}
