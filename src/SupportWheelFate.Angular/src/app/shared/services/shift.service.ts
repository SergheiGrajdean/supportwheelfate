import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';

import { Configuration } from '../../app.constants';

import { Shift } from '../models/shift.model';

@Injectable()
export class ShiftService {

  private actionUrl: string;
  private engineersUrl: string;

  constructor(private http: HttpClient, private configuration: Configuration) {
    this.actionUrl = configuration.ServerWithApiUrl + 'shifts/';
  }

  private handleError(error: any) {
    if (error instanceof Response) {
      return Observable.throw(error.json()['error'] || 'backend server error');
    }
    return Observable.throw(error || 'backend server error');
  }

  listAll(): Observable<Array<Shift>> {
    return this.http.get<Array<Shift>>(this.actionUrl);
  }

  getBySprint(sprintId): Observable<Array<Shift>> {
    return this.http
      .get<Array<Shift>>(this.getShiftsBySprintUrl(sprintId))
      .map((data: Array<Shift>) => {
        for (let shift of data) {
          shift.end = new Date('1970-01-01T' + shift.end + 'Z');
          shift.start = new Date('1970-01-01T' + shift.start+ 'Z');
        }
        return data;
    });
  }

  private getShiftsBySprintUrl(id: number): string {
    return this.configuration.ServerWithApiUrl + `sprints/${id}/shifts/`;
  }
}

