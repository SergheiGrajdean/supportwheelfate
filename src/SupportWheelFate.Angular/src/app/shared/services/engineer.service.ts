import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';

import { Configuration } from '../../app.constants';

import { Engineer } from '../models/engineer.model';

@Injectable()
export class EngineerService {

  private actionUrl: string;
  private engineersUrl: string;

  constructor(private http: HttpClient, private configuration: Configuration) {
    this.actionUrl = configuration.ServerWithApiUrl + 'engineers/';
  }

  private handleError(error: any) {
    if (error instanceof Response) {
      return Observable.throw(error.json()['error'] || 'backend server error');
    }
    return Observable.throw(error || 'backend server error');
  }

  listAll(): Observable<Array<Engineer>> {
    return this.http.get<Array<Engineer>>(this.actionUrl);
  }
}
