import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { extendMoment } from 'moment-range';

const { range } = extendMoment(moment);

@Injectable()
export class DateService {

  constructor() { }

  isWeekEnd(date: moment.Moment) {
    var weekDay = date.isoWeekday();

    return weekDay == 6 || weekDay == 7;
  }

  getDays(start: moment.Moment, end: moment.Moment): Array<moment.Moment> {
    var startOfWeek = moment(start).startOf('isoWeek');
    var endOfWeek = moment(end).endOf('isoWeek');
    var days = Array.from(range(startOfWeek, endOfWeek).by("day"));
    return days;
  }

  getWeeks(start: moment.Moment, end: moment.Moment): Array<Array<moment.Moment>> {
    var days = this.getDays(start, end);
    return this.chunkArray(days, 7);
  }


  private chunkArray(myArray, chunk_size) {
    var results = [];

    while (myArray.length) {
      results.push(myArray.splice(0, chunk_size));
    }

    return results;
  }


}

