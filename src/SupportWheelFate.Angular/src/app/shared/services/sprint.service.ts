import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import * as moment from 'moment';

import { Configuration } from '../../app.constants';
import { Sprint } from '../models/sprint.model';
import { Engineer } from '../models/engineer.model';
import { Shift } from '../models/shift.model';
import { CreateSprintRequest } from '../models/create-sprint-request.model';


@Injectable()
export class SprintService {

  private actionUrl: string;
  private engineersUrl: string;

  constructor(private http: HttpClient, private configuration: Configuration) {
    this.actionUrl = configuration.ServerWithApiUrl + 'sprints/';
    this.engineersUrl = configuration.ServerWithApiUrl + '{0}/engineers/';
  }

  private handleError(error: any) {
    if (error instanceof Response) {
      return Observable.throw(error.json()['error'] || 'backend server error');
    }
    return Observable.throw(error || 'backend server error');
  }

  listAll(): Observable<Array<Sprint>> {
    return this.http.get<Array<Sprint>>(this.actionUrl)
      .map(data => {
        data.forEach(x => {
          x.startDate = moment(x.startDate);
          x.endDate = moment(x.endDate);
        });
        return data;
      });
  }

  create(startDate: Date, endDate: Date): Observable<Sprint> {
    const sprintRequest = new CreateSprintRequest(startDate, endDate);
    return this.http.post<Sprint>(this.actionUrl, sprintRequest);
  }

  get(id): Observable<Sprint> {
    let sprint = this.http.get<Sprint>(this.actionUrl + id);
    let engineers = this.http.get<Engineer>(this.getEngineersUrl(id));
    let shifts = this.http.get<Shift>(this.getShiftsUrl(id));

    return Observable.forkJoin<Sprint>([sprint, engineers, shifts]).map(data => {
      var sprint = data[0] as Sprint;
      sprint.startDate = moment(sprint.startDate);
      sprint.endDate = moment(sprint.endDate);

      var engineers = data[1] as Array<Engineer>;
      var shifts = data[2] as Array<Shift>;

      for (let day of sprint.days) {
        day.date = moment(day.date);
        for (let engineerShift of day.shifts) {
          engineerShift.engineer = engineers.filter(x => x.id === engineerShift.engineerId)[0];
          engineerShift.shift = shifts.filter(x => x.id === engineerShift.shiftId)[0];
        }
      }
      
      return sprint;
    });
  }

  private getEngineersUrl(id: number): string {
    return this.actionUrl + `${id}/engineers/`;
  }

  private getShiftsUrl(id: number): string {
    return this.actionUrl + `${id}/shifts/`;
  }
}

