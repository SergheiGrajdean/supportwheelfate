import { Engineer } from './engineer.model';
import { Shift } from './shift.model';


export class EngineerDayShift {
  constructor(public id: number,
    public shiftId: number,
    public engineerId: number,
    public engineer: Engineer,
    public shift: Shift) {
  }
}

