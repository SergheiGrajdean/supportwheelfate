export class CreateSprintRequest {
  constructor(public startDate: Date,
    public endDate: Date) {
  }
}

