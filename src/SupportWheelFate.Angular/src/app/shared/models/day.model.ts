import { Moment } from 'moment';
import { EngineerDayShift } from './engineer-day-shift.model'


export class Day {
  constructor(public id: number,
    public date: Moment,
    public shifts: Array<EngineerDayShift>) {
  }
}

