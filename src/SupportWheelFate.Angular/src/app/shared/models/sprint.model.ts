import * as moment from 'moment';
import { Day } from './day.model';

export class Sprint {
  constructor(public id: number,
    public startDate: moment.Moment,
    public endDate: moment.Moment,
    public days: Array<Day>) {
  }
}

