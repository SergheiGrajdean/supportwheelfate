export class Shift {
  constructor(public id: number,
    public start: Date,
    public end: Date
  ) { }
};

