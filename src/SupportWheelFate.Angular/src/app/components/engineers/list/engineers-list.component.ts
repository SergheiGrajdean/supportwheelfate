import { Component, OnInit, Input } from '@angular/core';
import * as moment from 'moment';

import { EngineerService } from '../../../shared/services/engineer.service';

import { Engineer } from '../../../shared/models/engineer.model';

@Component({
  selector: 'engineers-list',
  templateUrl: './engineers-list.component.html',
  styleUrls: ['./engineers-list.component.css']
})
export class EngineersListComponent implements OnInit {

  engineers: Engineer[];

  constructor(public engineerService: EngineerService) {
  }

  ngOnInit(): void {
    this.engineerService.listAll().subscribe(data => {
      this.engineers = data;
    });
  }


}

