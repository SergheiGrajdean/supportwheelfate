import { Component, OnInit, Input } from '@angular/core';
import * as moment from 'moment';

import { Shift } from '../../../shared/models/shift.model';

import { ShiftService } from '../../../shared/services/shift.service';

@Component({
  selector: 'shifts-list',
  templateUrl: './shifts-list.component.html',
  styleUrls: ['./shifts-list.component.css']
})
export class ShiftsListComponent implements OnInit {

  shifts: Array<Shift>;

  constructor(public shiftsService: ShiftService) {
  }

  ngOnInit(): void {
    this.shiftsService.listAll().subscribe(data => {
      this.shifts = data;
    });
  }


}

