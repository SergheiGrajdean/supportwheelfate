import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { SprintService } from '../../../shared/services/sprint.service';
import { Sprint } from '../../../shared/models/sprint.model';

@Component({
  selector: 'create-sprint',
  templateUrl: './create-sprint.component.html',
  styleUrls: ['./create-sprint.component.css']
})
export class CreateSprintComponent {
  minDate = new Date(2017, 5, 10);
  maxDate = new Date(2018, 9, 15);

  bsValue: Date = new Date();
  bsRangeValue: any;

  errors: string[];

  constructor(private sprintService: SprintService, private router: Router) {
    var start = new Date();
    var end = new Date();
    end.setDate(start.getDate() + 13);

    this.bsRangeValue = [start, end];
    this.errors = [];
  }

  create() {
    this.errors = [];
    this.sprintService.create(this.bsRangeValue[0], this.bsRangeValue[1])
      .subscribe((sprint: Sprint) => {
        this.router.navigate(['/sprints/details', sprint.id]);
      }, errors => { this.handleValidationErrors(errors); });
  }

  private handleValidationErrors(errors) {
    console.log('Object.values(errors.error)', Object.values(errors.error));
    if (errors.status == 400) {
      Object.values(errors.error).forEach(x => {
        this.errors = this.errors.concat(x);
      });
    }
  }
}
