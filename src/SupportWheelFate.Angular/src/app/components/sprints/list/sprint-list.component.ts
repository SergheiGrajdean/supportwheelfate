import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { SprintService } from '../../../shared/services/sprint.service';

import { Sprint } from '../../../shared/models/sprint.model';
import { Shift } from '../../../shared/models/shift.model';
import { Day } from '../../../shared/models/day.model';

@Component({
  selector: 'sprint-list',
  templateUrl: './sprint-list.component.html',
  styleUrls: ['./sprint-list.component.css']
})
export class SprintListComponent implements OnInit {
  sprints: Array<Sprint>;

  constructor(public sprintService: SprintService, public router: Router) {
  }

  ngOnInit(): void {
    this.sprintService.listAll().subscribe(data => {
      this.sprints = data;
    });
  }

}

