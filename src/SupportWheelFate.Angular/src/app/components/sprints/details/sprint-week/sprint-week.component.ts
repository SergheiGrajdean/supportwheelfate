import { Component, OnInit, Input } from '@angular/core';
import * as moment from 'moment';

import { DateService } from '../../../../shared/services/date.service';

import { Shift } from '../../../../shared/models/shift.model';
import { Day } from '../../../../shared/models/day.model';
import { Sprint } from '../../../../shared/models/sprint.model';

export interface ISprintWeek {
  sprint: Sprint;
  shifts: Array<Shift>;
  dates: Array<moment.Moment>;
}

@Component({
  selector: 'sprint-week',
  templateUrl: './sprint-week.component.html',
  styleUrls: ['./sprint-week.component.css']
})
export class SprintWeekComponent {

  @Input() sprintWeek: ISprintWeek;

  constructor(public dateService: DateService) {
  }

  isInDateRange(date: moment.Moment): boolean {
    return date.isBetween(this.sprintWeek.sprint.startDate, this.sprintWeek.sprint.endDate, 'day', '[]');
  }
}

