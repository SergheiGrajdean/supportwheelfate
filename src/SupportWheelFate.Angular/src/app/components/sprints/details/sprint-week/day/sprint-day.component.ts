import { Component, OnInit, Input } from '@angular/core';
import * as moment from 'moment';

import { Day } from '../../../../../shared/models/day.model';

@Component({
  selector: 'sprint-day',
  templateUrl: './sprint-day.component.html',
  styleUrls: ['./sprint-day.component.css']
})
export class SprintDayComponent implements OnInit {

  @Input() days: Array<Day>;
  @Input() date: moment.Moment;
  @Input() shiftId: number;

  name: string;

  constructor() {
  }

  ngOnInit() {
    var day = this.days.filter(x => x.date.isSame(this.date, 'day'))[0];

    if (day) {
      var engineerShift = day.shifts.filter(x => x.shiftId === this.shiftId)[0];
      this.name = engineerShift.engineer.name;
    } else {
      this.name = "";
    }
  }
}

