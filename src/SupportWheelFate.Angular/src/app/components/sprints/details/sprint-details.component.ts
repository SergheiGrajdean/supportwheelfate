import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';

import { SprintService } from '../../../shared/services/sprint.service';
import { ShiftService } from '../../../shared/services/shift.service';
import { DateService } from '../../../shared/services/date.service';

import { Sprint } from '../../../shared/models/sprint.model';
import { Shift } from '../../../shared/models/shift.model';
import { ISprintWeek } from './sprint-week/sprint-week.component';

@Component({
  selector: 'sprint-details',
  templateUrl: './sprint-details.component.html',
  styleUrls: ['./sprint-details.component.css']
})
export class SprintDetailsComponent implements OnInit {
  sprint: Sprint;
  shifts: Array<Shift>;
  weeks: Array<Array<moment.Moment>>;

  id: number;
  private sub: any;

  constructor(private sprintService: SprintService,
    private shiftService: ShiftService,
    private route: ActivatedRoute,
    private dateService: DateService) {
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = + params['id'];

      this.shiftService.getBySprint(this.id).subscribe((shifts: Array<Shift>) => {
        this.shifts = shifts;
      });

      this.sprintService.get(this.id).subscribe((sprint: Sprint) => {
        this.sprint = sprint;
        this.weeks = this.dateService.getWeeks(sprint.startDate, sprint.endDate);
      });
    });
  }

  getSprintWeek(week: moment.Moment[]): ISprintWeek {
    return {
      dates: week,
      sprint: this.sprint,
      shifts: this.shifts
    };
  }
}

