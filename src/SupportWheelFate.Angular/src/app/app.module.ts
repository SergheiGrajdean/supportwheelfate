import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AlertModule, CollapseModule } from 'ngx-bootstrap';
import { BsDatepickerModule, DatepickerModule } from 'ngx-bootstrap/datepicker';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';

import { Configuration } from './app.constants';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HomeComponent } from './components/home/home.component';
import { NgHttpLoaderModule } from 'ng-http-loader/ng-http-loader.module'; 

import { CreateSprintComponent } from './components/sprints/create/create-sprint.component';
import { SprintDetailsComponent } from './components/sprints/details/sprint-details.component';
import { SprintListComponent } from './components/sprints/list/sprint-list.component';
import { SprintWeekComponent } from './components/sprints/details/sprint-week/sprint-week.component';
import { SprintDayComponent } from './components/sprints/details/sprint-week/day/sprint-day.component';
import { EngineersListComponent } from './components/engineers/list/engineers-list.component';
import { ShiftsListComponent } from './components/shifts/list/shifts-list.component';


import { DateService } from './shared/services/date.service';
import { SprintService } from './shared/services/sprint.service';
import { ShiftService } from './shared/services/shift.service';
import { EngineerService } from './shared/services/engineer.service';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    CreateSprintComponent,
    SprintDayComponent,
    SprintWeekComponent,
    SprintDetailsComponent,
    SprintListComponent,
    EngineersListComponent,
    ShiftsListComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    DatepickerModule.forRoot(),
    BsDatepickerModule.forRoot(),
    CollapseModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    NgHttpLoaderModule,
    FormsModule
  ],
  providers: [SprintService, ShiftService, Configuration, EngineerService, DateService],
  bootstrap: [AppComponent]
})
export class AppModule { }
