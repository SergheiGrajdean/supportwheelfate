import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { CreateSprintComponent } from './components/sprints/create/create-sprint.component';
import { SprintDetailsComponent } from './components/sprints/details/sprint-details.component';
import { SprintListComponent } from './components/sprints/list/sprint-list.component';
import { EngineersListComponent } from './components/engineers/list/engineers-list.component';
import { ShiftsListComponent } from './components/shifts/list/shifts-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'sprints/create', component: CreateSprintComponent },
  { path: 'sprints/list', component: SprintListComponent },
  { path: 'sprints/details/:id', component: SprintDetailsComponent },
  { path: 'engineers/list', component: EngineersListComponent },
  { path: 'shifts/list', component: ShiftsListComponent },
  { path: '**', redirectTo: 'home' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
