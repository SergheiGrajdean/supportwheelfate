import { Injectable } from '@angular/core';

@Injectable()
export class Configuration {
  public Server: string;
  public ApiUrl: string;
  public ServerWithApiUrl: string;

  constructor() {
    if (window && (<any>window).__env) {
      var env = (<any>window).__env;
      this.Server = env.serverUrl;
      this.ApiUrl = env.apiUrl;
      this.ServerWithApiUrl = this.Server + this.ApiUrl;
    } else {
      throw new Error('window.__env variablie not defined');
    }
  }
}
